using System;
using UnityEngine;



public class ParallaxEffect : MonoBehaviour
{
    [Serializable]//Makes it so that it shows fields in the inspector
    //Only parallaxefect class can use this struct
    private struct QuadScroller
    {
        public Renderer SourceRenderer;
        public float scrollSpeed;
        public float moveSpeed;
    }

    [SerializeField] private QuadScroller[] quadScrollers;
    [SerializeField] private Transform targetToFollow; //Target that scenery follows. Not game object because we dont need all the info from gameObject

    void Update()
    {
        //each scroller corresponds to each quad
        foreach(var scroller in quadScrollers)
        {
            float offset = 0;
            offset += targetToFollow.position.x*scroller.moveSpeed; //moves based on player position
            offset += Time.time * scroller.scrollSpeed; //moves based on time when player is not moving
           // offset *= Time.deltaTime; //Makes frame rate independent
            scroller.SourceRenderer.material.mainTextureOffset = new Vector2(offset,0);
           
        }
    }
}
