using UnityEngine;

public class PlatformerMovement : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private SpriteRenderer spriteRenderer;

    [Header("Movement Config")]
    [SerializeField] private int movementSpeed;
    [SerializeField] private int jumpForce;

    private Rigidbody2D rb;
    private Animator animator;

    private bool canJump;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (canJump && Input.GetButtonDown("Jump"))
        {
            canJump = false;
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);

            animator.SetTrigger("Jump");
        }


    }

    private void FixedUpdate()
    {
        float xAxisRaw = Input.GetAxis("Horizontal");
        float horizontalMovement = xAxisRaw * movementSpeed * Time.fixedDeltaTime;

        rb.velocity = new Vector2(horizontalMovement, rb.velocity.y);

        if (Mathf.Abs(rb.velocity.x) > Mathf.Epsilon)
        {

            if (xAxisRaw < 0)
            {
                spriteRenderer.flipX = true;
            }
            else if (xAxisRaw > 0)
            {
                spriteRenderer.flipX = false;
            }

            animator.SetBool("isRunning", true);
        }
        else
        {
            animator.SetBool("isRunning", false);
        }

        if (rb.velocity.y < -0.5)
        {
            animator.SetBool("isFalling", true);
        }
        else
        {
            animator.SetBool("isFalling", false);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        canJump = true;
    }
}
