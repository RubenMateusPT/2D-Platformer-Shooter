using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTargetFollow : MonoBehaviour
{
    // Start is called before the first frame update

    //[SerializeField] private Camera mainCamera;
    [SerializeField] Transform targetToFollow;
    [SerializeField] bool isTrackingVertical;

    // Update is called once per frame
    void LateUpdate()
    {
        if (isTrackingVertical)
        {
            transform.position = new Vector3(targetToFollow.position.x, targetToFollow.position.y, transform.position.z);
        }
        else
        {
            transform.position = new Vector3(targetToFollow.position.x, transform.position.y, transform.position.z);
        }
    }
}
